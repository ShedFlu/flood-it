import React from 'react';
import ReactDOM from 'react-dom';
import logo from './logo.svg';
import './App.css';
import { tsConstructorType } from '@babel/types';
import { createSecureContext } from 'tls';

function Square(props) {
  return(
    <button className="square" className={props.color} onClick={props.onClick}></button>
  )
}

function checkTouching(props){
 
  return(
  "i"
  );
}

class InputFields extends React.Component {
  constructor(props) {
    super(props);
    this.state = {value: ''};

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({value: event.target.value});
  }

  handleSubmit(event) {
    alert('A name was submitted: ' + this.state.value);
    event.preventDefault();
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <label>
          Feldgröße(quadratisch):
          <input type="number" value={this.state.value} onChange={this.handleChange} />
        </label>
        <input type="submit" value="Submit" />
      </form>
    );
  }
}

class Board extends React.Component{
  renderSquare(i){
    return(
      <Square
      background-color={this.props.squares[i]}
      onClick={() => this.props.onClick(i)}
      />
    );
  }

  render() {
    return(
      {}
    );
  }
}

class Game extends React.Component{
  constructor() {
    constructor(props);
      {
        super(props);
        this.state = {
          stepNumber: 0,
          color: 0,
        };
      }
  }

  handleClick(i) {
    const history = this.state.history.slice(0, this.state.stepNumber + 1);
    const current = history[history.length - 1];
    const squares = current.squares.slice();
  }

  render() {
    const history = this.state.history;
    const current = history[this.state.stepNumber]

    return (
    <div className="game">
        <div className="gameSettings">
          <InputFields


          />
        </div>
        <div className="gameBoard">
          <Board
            squares{current.color}
            onClick={(i) => this.handleClick(i)}
          />
        </div>
        <div className="gameInfo">
      
        </div>
      </div>
    );
  }
}
export default Game;
